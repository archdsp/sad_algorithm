﻿#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define OPENCV

#ifdef OPENCV
// DEBUG
#include <opencv2/opencv.hpp>
#endif

int calculate_SAD(const struct mat* m1, const struct mat* m2);
int select_lowest_SAD_picture(struct mat* image_frames[4], struct mat* template_img, int tl_x, int tl_y, int br_x, int br_y);
struct mat* read_raw_color_image_as_grey_image(FILE* imgFile, int ch, int width, int height);
struct mat* crop_image(struct mat* img, int tl_x, int tl_y, int br_x, int br_y);
struct mat* create_mat(int ch, int row, int col);
void print_mat(struct mat* m);

void test_caculate_SAD();

struct mat
{
	int ch;
	int row;
	int col;
	unsigned char* data;
};

int main(void)
{
	FILE* pImgFile1 = fopen("1.raw", "rb");
	FILE* pImgFile2 = fopen("2.raw", "rb");
	FILE* pImgFile3 = fopen("3.raw", "rb");
	FILE* pImgFile4 = fopen("4.raw", "rb");
	FILE* pImgFile5 = fopen("5.raw", "rb");

	int ch = 3;  
	int width = 496;
	int height = 240;
	int image_candidates_count = 5;

	// 임의로 정한 1.dng 이미지에서의 소화기 위치입니다. tl은 좌상귀, br이 우하귀입니다.
	int roi_tl_x = 40, roi_tl_y = 120;
	int roi_br_x = 80, roi_br_y = 160;

	struct mat* image_frames[5] = {NULL,  NULL, NULL, NULL, NULL};

	// 참조로 쓸 프레임들을 읽어 옵니다.
	image_frames[0] = read_raw_color_image_as_grey_image(pImgFile1, ch, width, height);
	image_frames[1] = read_raw_color_image_as_grey_image(pImgFile2, ch, width, height);
	image_frames[2] = read_raw_color_image_as_grey_image(pImgFile3, ch, width, height);
	image_frames[3] = read_raw_color_image_as_grey_image(pImgFile4, ch, width, height);
	image_frames[4] = read_raw_color_image_as_grey_image(pImgFile5, ch, width, height);

	// 첫번째 프레임에서 소화기 위치 이미지만 크롭을 합니다.
	struct mat* template_img = crop_image(image_frames[0], roi_tl_x, roi_tl_y, roi_br_x, roi_br_y);

	// 첫번째 프레임에서 템플릿 이미지를 가져오니, 코드가 맞다면 무조건 아래의 결과는 0가 됩니다.
	// 따라서 아래의 함수 호출 결과 출력되는 이미지들의 SAD 값 들 중 두번째로 작은 값이 실제 정답이 됩니다.
	int bestone = select_lowest_SAD_picture(image_frames, template_img, roi_tl_x, roi_tl_y, roi_br_x, roi_br_y);
	printf("Image[%d] was selected\n", bestone);

	return bestone;
}

int select_lowest_SAD_picture(struct mat* image_frames[5], struct mat* template_img, int tl_x, int tl_y, int br_x, int br_y)
{
	// SAD 값과 위치 저장할 변수들
	int lowest_SAD = 9999999, lowest_x = -1, lowest_y = -1, temp_SAD = -1, bestimg = -1;
	int SAD = 9999999, x = -1, y = -1;

	// 인덱스 변수
	int k = 0, i = 0, j = 0;
	int frames_cnt = 5;

	// 검색 영역 확장정도
	int extend = 2;

	// Search Region을 extend 값 만큼 영역을 확장합니다.
	int search_region_tl_x = tl_x > extend ? tl_x - extend : 0;
	int search_region_tl_y = tl_y > extend ? tl_y - extend : 0;
	int search_region_br_x = br_x + extend < image_frames[k]->col && image_frames[k] !=NULL? br_x + extend : image_frames[k]->col - 1;
	int search_region_br_y = br_y + extend < image_frames[k]->row && image_frames[k] != NULL? br_y + extend : image_frames[k]->row - 1;

	printf("search area tl[%d, %d, %d, %d]\n", search_region_tl_x, search_region_tl_y, search_region_br_x, search_region_br_y);

	// 4개의 후보군 이미지마다 SAD 값을 계산합니다.
	for (k = 0; k < frames_cnt; k++)
	{
		struct mat* img = image_frames[k];
		if (img == NULL)
		{
			printf("NULL ERROR arised. Image not properly load\n");
			return -1;
		}

		// 후보군 이미지의 설정된 탐색 영역내에서
		for (i = search_region_tl_y; i <= search_region_br_y - template_img->row; i++)
		{
			for (j = search_region_tl_x; j <= search_region_br_x - template_img->col; j++)
			{
				// 현재 원본 이미지 블록에서 template 이미지 영역만큼을 크롭합니다.
				struct mat* current_block = crop_image(img, j, i, j + template_img->col, i + template_img->row);

#ifdef OPENCV
				cv::Mat can(img->row, img->col, CV_8UC1, img->data);
				cv::cvtColor(can, can, cv::COLOR_GRAY2BGR);
				cv::rectangle(can, cv::Point(search_region_tl_x, search_region_tl_y), cv::Point(search_region_br_x, search_region_br_y), cv::Scalar(255, 255, 0), 1);
				cv::rectangle(can, cv::Point(j, i), cv::Point(j + template_img->col, i + template_img->row), cv::Scalar(0, 255, 0), 1);
				cv::Mat block(current_block->row, current_block->col, CV_8UC1, current_block->data);
				cv::imshow("BLOCK", block);
				cv::imshow("CANIDATES", can);
#endif
				// 현재 블록과 template 이미지간의 SAD 값을 계산하고, 이전 SAD 값과 비교하여 
				// 가장 작은 SAD값을 찾도록 합니다
				temp_SAD = calculate_SAD(template_img, current_block);

				if (temp_SAD >= 0 && temp_SAD < SAD) 
				{
					SAD = temp_SAD;
					x = j;
					y = i;
				}
#ifdef OPENCV
				if (cv::waitKey(100) == 27)
					goto EXIT;
#endif
			}
		}
#ifdef OPENCV
		EXIT:
#endif
		printf("Image [%d] = SAD %d,  [tl : %d, %d, br : %d %d]\n", k, SAD, x, y, x + template_img->col, y + template_img->row);
		if (SAD < lowest_SAD)
		{
			lowest_SAD = SAD;
			lowest_x = x;
			lowest_y =y;
			bestimg = k;
		}
		SAD = 9999999, x = -1, y = -1;
	}
	printf("Best Image : %dth one. SAD = %d,  x, y = [%d, %d]\n", bestimg, lowest_SAD, lowest_x, lowest_y);
	return bestimg;
}

int calculate_SAD(const struct mat* m1, const struct mat* m2)
{
	// 만일 두 이미지의 크기가 다르다면 계산할 수 없으므로 -1 값을 리턴합니다.
	if (m1->row != m2->row && m1->col != m2->col)
		return -1;

	int SAD = 0;
	int i = 0, j = 0;

	for (i = 0; i < m1->col; i++)
		for (j = 0; j < m1->row; j++)
			SAD += abs(m1->data[i * m1->col + j] - m2->data[i * m2->col + j]);

	return SAD;
}

struct mat* crop_image(struct mat* img, int tl_x, int tl_y, int br_x , int br_y)
{
	int crop_width = br_x - tl_x;
	int crop_height = br_y - tl_y;
	int  i = tl_y, j = tl_x, k = 0;
	struct mat* crop = create_mat(1, crop_height, crop_width);

	for (i = tl_y; i < br_y; i++)
		for (j = tl_x; j < br_x; j++)
			crop->data[k++] = img->data[i * img->col + j];

	return crop;
}

// 여기서 raw 이미지란, 헤더가 없는 이미지 원본 데이터 그 자체를 말함니다.
// 1차원 배열로 각각 R, G, B 순서대로 픽셀이 존재합니다
struct mat* read_raw_color_image_as_grey_image(FILE* imgFile,  int ch, int width, int height)
{
	if (imgFile == NULL)
	{
		printf("No image file opend");
		return NULL;
	}

	struct mat* grey = create_mat(1, height, width);
	unsigned char buff[3] = { 0, 0, 0 };
	int i = 0;

	while (fread(buff, sizeof(unsigned char), ch, imgFile) > 0)
	{
		unsigned char R = buff[0];
		unsigned char G = buff[1];
		unsigned char B = buff[2];

		grey->data[i++] = (R + G + B) / 3;
	}

	return grey;
}

struct mat* create_mat(int ch, int row, int col)
{
	struct mat* m = (struct mat*)malloc(sizeof(struct mat));
	m->ch = ch;
	m->row = row;
	m->col = col;
	m->data = (unsigned char*)malloc(sizeof(unsigned char) * row * col * ch);
	return m;
}

void print_mat(struct mat* m)
{
	int i = 0, j = 0;
	for (i = 0; i < m->col; i++)
	{
		for (j = 0; j < m->row; j++)
			printf("%4d", m->data[i * m->col + j]);
		printf("\n");
	}
}

/* Below only complete test code. always has form like 'void test_VERB_NOUN(void)' */

void test_caculate_SAD()
{
	struct mat* template_img = create_mat(1, 3, 3);
	struct mat* original_img = create_mat(1, 3, 3);
	
	unsigned char  tem[3][3] = {
	{2, 5, 5},
	{4, 0, 7},
	{7, 5, 9}
	};

	unsigned char search1[3][3] = {
	{2, 7, 5},
	{1, 7, 4},
	{8, 4, 6}
	};

	unsigned char search2[3][3] = {
{7, 5, 8},
{7, 4, 2},
{4, 6, 8}
	};

	unsigned char search3[3][3] = {
{5, 8, 6},
{4, 2 ,7},
{6, 8 ,5}
	};

	memcpy(template_img->data, tem, sizeof(tem));
	memcpy(original_img->data, search1, sizeof(search1));
	printf("LEFT = %d\n", calculate_SAD(template_img, original_img));
	memcpy(original_img->data, search2, sizeof(search2));
	printf("CENTRE = %d\n", calculate_SAD(template_img, original_img));
	memcpy(original_img->data, search3, sizeof(search3));
	printf("RIGHT = %d\n", calculate_SAD(template_img, original_img));
	return;
}